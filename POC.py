import httplib
import time
import subprocess
import os
import sys

if ('--debug' in sys.argv) or ('-d' in sys.argv):
    debug = True

try:
    vf = open('LAST_CHANGED', 'r+')
    versionfile = True
    print('file found')
except Exception as e:
    if e.args[0] == 2:
        print('File not found. Creating file...')
        vf = open('LAST_CHANGED', 'w+')
    versionfile = False


connection = httplib.HTTPConnection('commondatastorage.googleapis.com')
connection.connect()
connection.request('get', '/chromium-browser-continuous/Win_x64/LAST_CHANGE')
response = connection.getresponse()

print(response.status)
version = int(response.read())

if not versionfile:
    old_version = int(version)
    vf.write(str(version)+'\n')
    print('No versionfile. Latest version copied.')

else:
    vf.seek(0)
    readbuffer = vf.readline()
    if readbuffer:
        print('Old version read from versionfile:')
        old_version = readbuffer
        print(old_version)
        vf.seek(0)
        vf.truncate()
        vf.write(str(version)+'\n')
    else:
        print('Could not read anything')
        old_version = version
        vf.seek(0)
        vf.truncate()
        vf.write(str(version)+'\n')
        versionfile = False


vf.close()

if versionfile:
    print('Latest version: '+str(version)+' <==> Installed version: '+str(old_version))
else:
    print('Latest version: '+str(version))
time.sleep(2)


if (int(version) > int(old_version)) or (not versionfile):

        connection.request('get', '/chromium-browser-continuous/Win_x64/'+str(version)+'/mini_installer.exe')
        response = connection.getresponse()

        file_size = int(response.getheader("Content-Length"))
        print(file_size)
        file_size_dl = 0

        block_sz = 16384*16
        f = open('mini_installer.exe', 'wb')

        while True:
                buffer = response.read(block_sz)
                if not buffer:
                        break

                file_size_dl += len(buffer)
                f.write(buffer)

                status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl*100./file_size)
                #status = status + chr(8)*(len(status)+1)
                os.system('cls' if os.name == 'nt' else 'clear')
                print status


        f.close()
        print('Trying to execute mini_installer')

        subprocess.call(['.\\mini_installer.exe'])
        if debug:
            print('Opening install log...')
            subprocess.call(['notepad.exe', os.getenv('TEMP')+'\\chrome_installer.log'])
else:
        print('No newer version')
